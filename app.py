from flask import Flask, jsonify, request, Response
import json
from settings import *
from DatabaseModel import *
#from UserModel import User

import exchange_conn


def check_connection(exchange):
    if (exchange == 'poloniex'):
        polo_connection = exchange_conn.get_polo_conn()
        if (polo_connection != False):
            return True
        else:
            return False
    if (exchange == 'bitfinex'):
        bitf_connection = bitf_conn.get_bitf_conn()
        if (bitf_connection != False):
            return True
        else:
            return False

def is_valid_get_exchange_obj(request_data):
    if ('exchange' in request_data):
        exchange = request_data['exchange']
        if (exchange == 'poloniex' or exchange == 'bitfinex' or exchange == 'bittrex'):
            return True
        else:
            return False
    else:
        return False

# Start of route definitions

# get /conn_test
# {
#     'exchange': 'poloniex'
# } 
@app.route('/conn_test', methods=['POST'])
def conn_test():
    request_data = request.get_json()
    exchange = request_data['exchange'] 
    if (exchange == 'bitfinex'):
        connection = exchange_conn.get_bitf_conn()
    elif (exchange == 'poloniex'):
        connection = exchange_conn.get_polo_conn()
    else:
        return "Invalid exchange"

    if (connection != False):
        conn_msg = 'Connection OK: ' + exchange
        return conn_msg
    else:
        conn_msg = 'Connection Error'
        return conn_msg

# POST /put_balances_to_db
# {
#     'exchange': 'poloniex'
# } 
@app.route('/load_balances_to_db', methods=['POST'])
def load_balances():
    request_data = request.get_json()
    if (is_valid_get_exchange_obj(request_data)):
        exchange = request_data['exchange']
        if (check_connection(exchange)):
            if (exchange == 'poloniex'):
                polo_connection = exchange_conn.get_polo_conn()
                balances = polo_connection.returnBalances()
                print(type(balances))
                for asset, amount in balances.items():
                    if (amount != 0.0):
                        print(asset, amount)
                        Balance.add_balance(asset, exchange, amount)
                return jsonify(dict(balances))
        else:
            return "Connection Error: " + exchange
    else:
        invalid_request_obj_err_msg= {
            'error': 'Invalid request object',
            'help': 'Check JSON KV pairs'
        }
        response = Response(json.dumps(invalid_request_obj_err_msg), status=400, mimetype='application/json')
        return response

# GET /get_balances_from_db
@app.route('/get_balances_from_db')
def get_balances():
    return jsonify({'balances': Balance.get_all_balances()})

# POST /get_exchange_balances_from_db
# {
#     'exchange': 'poloniex'
# } 
@app.route('/get_exchange_balances_from_db', methods=['POST'])
def get_exchange_balances_from_db():
    request_data = request.get_json()
    exchange = request_data['exchange']
    if (is_valid_get_exchange_obj):
        return jsonify({'balances': Balance.get_exchange_balances(exchange)})

# POST /load_prices
# {
#     'exchange': 'poloniex'
# } 
@app.route('/load_latest_prices_to_db', methods=['POST'])
def load_latest_prices_to_db():
    request_data = request.get_json()
    if (is_valid_get_exchange_obj(request_data)):
        exchange = request_data['exchange']
        if (check_connection(exchange)):
            if (exchange == 'poloniex'):
                polo_connection = exchange_conn.get_polo_conn()
                prices = polo_connection.returnTicker()
                pricesDict = dict(prices)
                for symbol, values in pricesDict.items():
                    Latest_Price.add_price(symbol, exchange, values['last'], values['lowestAsk'], values['highestBid'])
                return jsonify(str(prices))
        else:
            return "Connection Error: " + exchange
    else:
        invalid_request_obj_err_msg= {
            'error': 'Invalid request object',
            'help': 'Check JSON KV pairs'
        }
        response = Response(json.dumps(invalid_request_obj_err_msg), status=400, mimetype='application/json')
        return response

# GET /get_latest_prices_from_db
@app.route('/get_latest_prices_from_db')
def get_prices_from_db():
    return jsonify({'prices': Latest_Price.get_all_prices()})

# POST /get_exchange_latest_prices_from_db
# {
#     'exchange': 'poloniex'
# } 
@app.route('/get_exchange_latest_prices_from_db', methods=['POST'])
def get_exchange_symbol_pricefrom_db():
    request_data = request.get_json()
    exchange = request_data['exchange']
    if (is_valid_get_exchange_obj):
        return jsonify({'latest_prices': Latest_Price.get_exchange_prices(exchange)})

# POST /get_symbol_latest_price_from_db
# {
#     'exchange': 'poloniex'
# } 
@app.route('/get_symbol_latest_price_from_db', methods=['POST'])
def get_symbol_latest_price_from_db():
    request_data = request.get_json()
    symbol = request_data['symbol']
    exchange = request_data['exchange']
    if (is_valid_get_exchange_obj):
        return jsonify({'latest_price': Latest_Price.get_symbol_price(symbol, exchange)})


# POST /get_asset_balance_from_db
# {
#     'exchange': 'poloniex'
#     'asset': 'BTC'
# } 
@app.route('/get_asset_balance_from_db', methods=['POST'])
def get_asset_balance_from_db():
    request_data = request.get_json()
    symbol = request_data['asset']
    exchange = request_data['exchange']
    if (is_valid_get_exchange_obj):
        return jsonify({'asset_balance': Balance.get_asset_balance(symbol, exchange)})

app.run(port=5000)

