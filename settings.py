from flask import Flask

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/chrismilner/Bluemoon_Consulting/Projects/venvs/et4/database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
