from poloniex import Poloniex
from exchange_settings import POLO_KEY, POLO_SECKEY
from bitfinex import WssClient, ClientV2, ClientV1
import os

# @desc Open a connection to Poloniex exchange
# @return polConn, a open tcp connection to Poloniex exhange
def get_polo_conn():
    polo_conn = Poloniex(POLO_KEY, POLO_SECKEY)
    polo_conn.returnBalances()
    try:
        polo_conn.returnBalances()
        return polo_conn
    except:
        return False

def get_bitf_conn():
    bitf_conn = ClientV2(os.environ.get('BITF_KEY'),os.environ.get('BITF_SECKEY'))
    try:
        return str(bitf_conn.platform_status())
    except:
        return False
