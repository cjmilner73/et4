from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import json
from settings import app

db = SQLAlchemy(app)

class Latest_Price(db.Model):
    __tablename__ = 'latest_prices'
    symbol = db.Column(db.String(12), primary_key=True)
    exchange = db.Column(db.String(30), primary_key=True)
    last = db.Column(db.Float, nullable=False)
    lowestAsk = db.Column(db.Float, nullable=False)
    highestBid = db.Column(db.Float, nullable=False)

    def json(self):
        return {'symbol': self.symbol, 'exchange':self.exchange, 'last': self.last, 
            'lowestAsk':self.lowestAsk, 'highestBid': self.highestBid}

    def add_price(_symbol, _exchange, _last, _lowestAsk, _highestBid):
        new_symbol = Latest_Price(symbol=_symbol, exchange=_exchange, last=_last, 
            lowestAsk=_lowestAsk, highestBid=_highestBid)
        db.session.add(new_symbol)
        db.session.commit()

    def get_all_prices():
        return [Latest_Price.json(latest_price) for latest_price in Latest_Price.query.all()]

    def get_exchange_prices(_exchange):
        return [Latest_Price.json(latest_price) for latest_price in Latest_Price.query.filter_by(exchange=_exchange)]

    def get_symbol_price(_symbol, _exchange):
        return Latest_Price.json(Latest_Price.query.filter_by(symbol=_symbol).filter_by(exchange=_exchange).first())

class Balance(db.Model):
    __tablename__ = 'balance'
    asset = db.Column(db.String(8), primary_key=True)
    exchange = db.Column(db.String(30), primary_key=True)
    amount = db.Column(db.Float, nullable=False)

    def json(self):
        return {'asset': self.asset, 'exchange':self.exchange, 'amount':self.amount}
        
    def add_balance(_asset, _exchange, _amount):
        new_asset = Balance(asset=_asset, exchange=_exchange, amount=_amount)
        db.session.add(new_asset)
        db.session.commit()

    def get_all_balances():
        return [Balance.json(balance) for balance in Balance.query.all()]

    def get_exchange_balances(_exchange):
        return [Balance.json(balance) for balance in Balance.query.filter_by(exchange=_exchange)]

    def get_asset_balance(_asset, _exchange):
        rows_returned = Balance.query.filter_by(asset=_asset).filter_by(exchange=_exchange).first()
        if rows_returned is None:
            return {}
        else:
            return Balance.json(rows_returneD)

    def delete_book(_isbn):
        is_successful = Book.query.filter_by(isbn=_isbn).delete()
        db.session.commit()
        return bool(is_successful)

    def update_book_price(_isbn, _price):
        book_to_update = Book.query.filter_by(isbn=_isbn).first()
        if ( book_to_update != None):
            print(book_to_update)
            print(type(book_to_update))
            book_to_update.price = _price
            db.session.commit()

    def update_book_name(_isbn, _name):
        book_to_update = Book.query.filter_by(isbn=_isbn).first()
        book_to_update.name = _name
        db.session.commit()
        
    def replace_book(_isbn, _name, _price):
        book_to_replace = Book.query.filter_by(isbn=_isbn).first()
        book_to_replace.price = _price
        book_to_replace.name = _name
        db.session.commit()


    def __repr__(self):
        balance_object = {
            'asset': self.asset,
            'exchange': self.exchange,
            'amount': self.amount
        }
        return json.dumps(balance_object)
